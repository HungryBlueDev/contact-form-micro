import os
import json
import requests
import bleach
from flask import Flask, render_template, request

from send_mail import send_mail

_BOT_IDENTIFIED = 'OK'

app = Flask(__name__)


def is_bot(captcha_response):
    secret = os.getenv('GOOGLE_RECAPTCHA_KEY')
    payload = {'response': captcha_response, 'secret': secret}
    response = requests.post(
        "https://www.google.com/recaptcha/api/siteverify", payload)
    response_text = json.loads(response.text)
    return not response_text['success']


@app.route('/', methods=["GET"])
def hello_world():
    return render_template('main.html', captcha_client_key=os.getenv('GOOGLE_PUBLIC_KEY'))


@app.route('/contactform', methods=["POST"])
def respond_to_formsubmit():
    trap = request.form['extrainfo']
    if trap.strip() != '':
        return _BOT_IDENTIFIED

    captcha = request.form['g-recaptcha-response']
    if is_bot(captcha_response=captcha):
        return _BOT_IDENTIFIED

    name = bleach.clean(request.form['name'])
    email = bleach.clean(request.form['email'])
    subject = bleach.clean(request.form['subject'])
    details = bleach.clean(request.form['details'])

    mail_info = {
        "name": name,
        "email": email,
        "subject": subject,
        "details": details,
    }

    send_mail(mail_info)

    return render_template('accepted.html')


if __name__ == "__main__":
    app.run()
