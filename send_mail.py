import smtplib
import ssl
import os

from email.message import EmailMessage

_SMTP_SERVER_DOMAIN = 'smtp.gmail.com'
_SMTP_SERVER_PORT = 587


def retrieve_credentials():
    try:
        sender_email = os.getenv('SENDER_EMAIL')
        password = os.getenv('PASSWORD')
        receiver_email = os.getenv('RECEIVER_EMAIL')
    except:
        print('Could not retrieve credentials. Make sure environment variables are initialized.')
        exit(1)
    return sender_email, password, receiver_email


def prepare_email(mail_info, sender_email, receiver_email):
    msg = EmailMessage()

    content = f'Name: {mail_info["name"]}\n' \
        f'Email: {mail_info["email"]}\n' \
        f'Subject: {mail_info["subject"]}\n\n' \
        f'Details:\n\n{mail_info["details"]}'

    msg.set_content(content)

    msg['Subject'] = f"Form input: {mail_info['subject']}"
    msg['From'] = sender_email
    msg['To'] = receiver_email

    return msg


def send_mail(mail_info):
    sender_email, password, receiver_email = retrieve_credentials()

    msg = prepare_email(mail_info, sender_email, receiver_email)

    smtp_server = _SMTP_SERVER_DOMAIN
    port = _SMTP_SERVER_PORT

    context = ssl.create_default_context()

    try:
        server = smtplib.SMTP(smtp_server, port)
        server.ehlo()
        server.starttls(context=context)
        server.ehlo()
        server.login(sender_email, password)
        server.send_message(msg)

    except Exception as e:
        print(e)
    finally:
        server.quit()
